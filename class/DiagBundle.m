classdef DiagBundle < FiberBundle
    properties
    end

    methods
        function obj = DiagBundle(FiberSim)
            obj.FiberSim = FiberSim;
            n = 5;

            rng(1);
            xycoord = (rand(n,2).* FiberSim.bundle_len)+FiberSim.padding;
  
            %xycoord = ([0.51,0.7;0.51,0.7].* FiberSim.bundle_len)+FiberSim.padding;

            obj.X = xycoord(:,1);
            obj.Y = xycoord(:,2);

            obj.r = repmat(FiberSim.r_core,n,1);
            obj.lum =  zeros(n,1);


            obj.lumnoise = ones(numel(obj.X),1);
           rng(1);
            obj.Permutation = randperm(n)';

            obj = obj.simulateBundle(ones(FiberSim.res_Pattern,FiberSim.res_Pattern,1));


            obj.lum_factor =  255/ max(obj.lum);

        end
    end

end