classdef LineAddSolver < Solver
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here

    properties

        % additive linesegment width in Source Pixels0
        slice_w = 8;
        lum_thresh = 250;
        

    end

    methods
        %constructor
        function obj = LineAddSolver(Bundle, FiberSim,fiber_d_frac,lum_thresh)
            alltemp = tic;
            obj.FiberSim  = FiberSim;
            obj.Bundle    = Bundle;
            obj.lum_thresh = lum_thresh;

            obj.SensorIm = zeros(obj.FiberSim.sensor_resolution,obj.FiberSim.sensor_resolution);
            obj.SourceIm = zeros(obj.FiberSim.res_Pattern,obj.FiberSim.res_Pattern);

            slice_w_temp = floor(fiber_d_frac * obj.FiberSim.d_fiber * obj.FiberSim.factor_to_pxls);

            if(slice_w_temp == 0)
                warning('fiber_d_frac too small. slice width set to 1px (source)');
                obj.slice_w  = 1;
            else
                obj.slice_w = slice_w_temp;
            end

            Coords = 1: obj.slice_w: obj.FiberSim.res_Pattern-obj.slice_w;


            % fiber positions on Proximal end
            % (generate empty LUT)
            obj.SourceIm(:) = 255;

            obj.Bundle.simulateBundle(obj.SourceIm);
            obj.SensorIm = obj.Bundle.SensorImage;

            % find Fiber Centers
            if ~obj.FiberSim.use_exact_fiberCenters
                [centers, radii] = obj.FDDT(obj.SensorIm);
            else
                centers = [obj.Bundle.X,obj.Bundle.Y] .* obj.FiberSim.factor_to_sensor_pxls;
                radii   = [obj.Bundle.r] .* obj.FiberSim.factor_to_sensor_pxls;
            end

            if(isempty(centers))
                error("no fiber centers found in sensor Image sensor resolution igit s too small ")
            end


            obj.DetectedVBundle = VirtualBundle(centers(:,1),centers(:,2),radii,zeros(length(centers),1),obj.FiberSim,obj.FiberSim.sensor_resolution);


            obj.SampleIndices = sub2ind( ...
                [obj.FiberSim.sensor_resolution, obj.FiberSim.sensor_resolution, 1], ...
                round(obj.DetectedVBundle.Y(:,1)), ...
                round(obj.DetectedVBundle.X(:,1)));




            % reset source to 0
            obj.SourceIm(:) = 0;

            X_distal  = zeros(numel(obj.DetectedVBundle.X),1);
            Y_distal  = zeros(numel(obj.DetectedVBundle.Y),1);
            lum_Bench = zeros(numel(obj.DetectedVBundle.Y),1);


            obj.ttSimBundle = 0;

            % Vertical Line --> x coords
            i = 0;
            obj.nPatternsProcessed = 0;
            for x = Coords

                obj.SourceIm(:,x:x+obj.slice_w) = 255;

                %imshow(obj.SourceIm)
                simbundletemp = tic;
                obj.Bundle = obj.Bundle.simulateBundle(obj.SourceIm);
                obj.nPatternsProcessed = obj.nPatternsProcessed +1;

                obj.SensorIm = obj.Bundle.SensorImage;
                obj.ttSimBundle = obj.ttSimBundle + toc(simbundletemp);

                % sample at fiber centers
                lum  = obj.SensorIm(obj.SampleIndices);

                I = (lum > obj.lum_thresh) & (lum_Bench == 0);

                lum_Bench(I) = 1;

                X_distal(I) = (x+obj.slice_w) * (obj.FiberSim.sensor_resolution / cast(obj.FiberSim.res_Pattern,'double'));
                if ~obj.FiberSim.headless
                    i = i +1;
                    obj.printStatus(i,numel(Coords)*2);
                end


            end

            %reset lum benchmark
            lum_Bench(:) = 0;
            obj.SourceIm(:) = 0;

            % Horizontal Line --> y coords
            for y = Coords

                obj.SourceIm(y:y+obj.slice_w,:) = 255;

                %imshow(obj.SourceIm)
                simbundletemp = tic;
                obj.Bundle = obj.Bundle.simulateBundle(obj.SourceIm);
                obj.nPatternsProcessed = obj.nPatternsProcessed +1;

                obj.SensorIm = obj.Bundle.SensorImage;
                obj.ttSimBundle = obj.ttSimBundle + toc(simbundletemp);

                % sample at fiber centers
                lum  = obj.SensorIm(obj.SampleIndices);

                I = (lum > obj.lum_thresh) & (lum_Bench == 0);

                lum_Bench(I) = 1;

                Y_distal(I) = (y+obj.slice_w) * (obj.FiberSim.sensor_resolution / cast(obj.FiberSim.res_Pattern,'double'));
                if ~obj.FiberSim.headless
                    i = i +1;
                    obj.printStatus(i,numel(Coords)*2);
                end



            end

            obj.ReconstructedVBundle = VirtualBundle( ...
                X_distal, ...
                Y_distal, ...
                obj.DetectedVBundle.r, ...
                zeros(obj.DetectedVBundle.len,1), ...
                obj.FiberSim, ...
                obj.FiberSim.sensor_resolution);

            obj.ttSimBundleNormal = obj.ttSimBundle;
            obj.ttKalib = toc(alltemp);
        end

    end
end