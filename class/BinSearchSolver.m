classdef BinSearchSolver < Solver
    properties

        %% Bin Search

        % Normal Reconstruction
        maxPow2
                
        ttKalibShift;
        ttSimBundleShift = 0;






        bar_width
        n_bars

        BinArrX
        BinArrY

        ReconstructedVBundle_Addr

        stdevThresh = 60;
        stdev

        % shift refinement
        stepsize
        circshiftSteps

        shiftLumX
        shiftLumY
        ReconstructedVBundle_shift
        nPatternsProcessedShift =0;
    end

    methods
       function lumdiff = genBinLumDiff(obj,SourceIm)
            
            firsttemp = tic;
            obj.Bundle = obj.Bundle.simulateBundle(SourceIm);
            obj.nPatternsProcessed = obj.nPatternsProcessed +1;
            obj.SensorIm = obj.Bundle.SensorImage;
            first = toc(firsttemp);
            % sample lum at fiber centers
            lum  = cast(obj.SensorIm(obj.SampleIndices),'double');

          
            
            %Comlementary Source Image 
            SourceIm(:) = (~SourceIm)*255;
            secondtemp = tic;
            obj.Bundle = obj.Bundle.simulateBundle(SourceIm);
            obj.nPatternsProcessed = obj.nPatternsProcessed +1;
            obj.SensorIm = obj.Bundle.SensorImage;
            second = toc(secondtemp);
 
            obj.ttSimBundle = obj.ttSimBundle + first + second;
            % sample lum at fiber centers
            lumCompl  = cast(obj.SensorIm(obj.SampleIndices),'double');


            obj.stdev =  std(cast(lum,'double'));

            lumdiff = lum - lumCompl;

            lumdiff(lumdiff < 0) = 0;
            
            lumdiff(lumdiff > 1) = 1;

       end

        function image = solveShift(obj,sourceImage,drawType)
            obj.Bundle = obj.Bundle.simulateBundle(sourceImage);

            obj.ReconstructedVBundle_shift.lum = obj.Bundle.SensorImage(obj.SampleIndices);


            switch drawType
                case 'draw'
                    image = obj.ReconstructedVBundle_shift.draw();
                case 'interpolate'
                    image = obj.ReconstructedVBundle_shift.interpolate();
                case 'gaussFold'
                    image = obj.ReconstructedVBundle_shift.gaussFold();
                case 'address'
                    image = obj.ReconstructedVBundle_shift.address();
                case 'address_vanilla'
                    image = obj.ReconstructedVBundle_shift.address_vanilla();

                case 'otherwise'
                    error('doesnt match any draw method');
            end

        end

        function [RealVersusEstimate] = correlateFibersShift(obj)

            if ~obj.FiberSim.use_exact_fiberCenters
                error('fibercenters on proximal side are not known');
            end

            Xcoords = obj.Bundle.X * obj.FiberSim.factor_to_sensor_pxls;
            Ycoords = obj.Bundle.Y * obj.FiberSim.factor_to_sensor_pxls;
            Permutatiuon = obj.Bundle.Permutation;

            %       seqNumber           distal          Proximal
            real = [(1:numel(Xcoords))',Xcoords(Permutatiuon),Ycoords(Permutatiuon),Xcoords,Ycoords];

            estimate = [(1:numel(obj.DetectedVBundle.X))',obj.DetectedVBundle.X,obj.DetectedVBundle.Y,obj.ReconstructedVBundle_shift.X,obj.ReconstructedVBundle_shift.Y];


            real     = sortrows(real,[4,5]);
            estimate = sortrows(estimate,[2,3]);



            estimate = [estimate,real];

            estimate = sortrows(estimate,6);

            RealVersusEstimate = [estimate(:,7:8),estimate(:,4:5)];

        end

        function RSME = getShiftRSME(obj)
                realVest = obj.correlateFibersShift;

                estimate = realVest(:,1:2);
                real     = realVest(:,3:4);

                RSME = sqrt(mean(vecnorm(estimate-real, 2, 2).^2));
        end

        function [ttcalibShift,ttcalibShiftClean] = getTTCalibShift(obj)
                ttcalibShift =  obj.ttKalibShift;
                ttcalibShiftClean = obj.ttKalibShift - (obj.ttSimBundleNormal+obj.ttSimBundleShift);
        end

        function nprocessd = getNprocessdShift(obj)
            nprocessd = obj.nPatternsProcessed + obj.nPatternsProcessedShift;
        
        end



    end

end