classdef Solver < handle


    properties
        FiberSim;
        Bundle;

        ReconstructedVBundle

        % detected fibers on proximal end
        DetectedVBundle
        SampleIndices


        r_core_factor = 0.60;
        disksize = 8;

        ttKalib = 0;

        SensorIm;
        SourceIm;

        ttSimBundle = 0;
        ttSimBundleNormal = 0;
        nPatternsProcessed = 0;
    end

    methods

        function [centers,r] = CHT(obj,image)

            [centers, r] = imfindcircles(image,[5,50],'ObjectPolarity','bright');

        end
        function [centers,r] = FDDT(obj,Image)

            r_core_pxl = obj.FiberSim.r_core * obj.FiberSim.factor_to_sensor_pxls;

            % thresholding of image
            level = graythresh(Image);
            bw = imbinarize(Image,level);

            % erosion and dilation

            %             stuct_disk = strel("disk",obj.disksize);
            %
            %
            %             bw =  imerode(bw,stuct_disk);
            %             bw =  imdilate(bw,stuct_disk);


            % Distance transform
            distIm = bwdist(~bw,'chessboard');



            characteristic_Fiber_R = r_core_pxl *obj.r_core_factor;
            distImthresh = distIm > characteristic_Fiber_R;
            %imshow(distImthresh)
            s = regionprops(distImthresh,'centroid');

            centers = cat(1,s.Centroid);
            %
            %             CC = bwconncomp(distImthresh);
            %             FiberCenterLUT = zeros(CC.NumObjects,1);
            %
            %
            %             % detect fiber centers from labeled regions
            %             for i = 1:CC.NumObjects
            %                 regionIndices = CC.PixelIdxList{1,i};
            %                 distVals = distIm(regionIndices);
            %                 [~,I] =max(distVals);
            %                 FiberCenterLUT(i,1) = regionIndices(I);
            %             end
            %
            %             % return Y and x coords of fber centers in sensor pixels
            %             [Y,X] = ind2sub(size(Image),FiberCenterLUT);
            %
            %             centers = [X,Y];

            r = repmat(r_core_pxl,length(centers),1);
        end

        function clamped = clampCoords(obj,coords)

            % maximum value is sensor res
            clamped = min(coords,obj.FiberSim.sensor_resolution);


            % minimum value is 1
            clamped = max(clamped,1);

        end

        function image = solveNormal(obj,sourceImage,drawType)
            obj.Bundle = obj.Bundle.simulateBundle(sourceImage);


            obj.ReconstructedVBundle.lum = obj.Bundle.SensorImage(obj.SampleIndices);



            switch drawType
                case 'draw'
                    image = obj.ReconstructedVBundle.draw();
                case 'draw_rect'
                    image = obj.ReconstructedVBundle.draw_rect(3);
                case 'interpolate'
                    image = obj.ReconstructedVBundle.interpolate();
                case 'gaussFold'
                    image = obj.ReconstructedVBundle.gaussFold();
                case 'address'
                    image = obj.ReconstructedVBundle.address();
                case 'address_vanilla'
                    image = obj.ReconstructedVBundle.address_vanilla();
                case 'address_fast'
                    image = obj.ReconstructedVBundle.address_fast();
                case 'prioNeighbor'
                    image = obj.ReconstructedVBundle.prioNeighbor();
                otherwise
                    error('doesnt match any draw method');
            end




        end

        function [RealVersusEstimate] = correlateFibers(obj)

            if ~obj.FiberSim.use_exact_fiberCenters
                error('fibercenters on proximal side are not known');
            end

            Xcoords = obj.Bundle.X * obj.FiberSim.factor_to_sensor_pxls;
            Ycoords = obj.Bundle.Y * obj.FiberSim.factor_to_sensor_pxls;
            Permutatiuon = obj.Bundle.Permutation;

            %       seqNumber           distal          Proximal
            real = [(1:numel(Xcoords))',Xcoords(Permutatiuon),Ycoords(Permutatiuon),Xcoords,Ycoords];

            estimate = [(1:numel(obj.DetectedVBundle.X))',obj.DetectedVBundle.X,obj.DetectedVBundle.Y,obj.ReconstructedVBundle.X,obj.ReconstructedVBundle.Y];


            real     = sortrows(real,[4,5]);
            estimate = sortrows(estimate,[2,3]);



            estimate = [estimate,real];

            estimate = sortrows(estimate,6);

            RealVersusEstimate = [estimate(:,7:8),estimate(:,4:5)];




        end
        function [ttcalib,ttcalibClean] = getTTCalib(obj)
            ttcalib =  obj.ttKalib;
            ttcalibClean = obj.ttKalib - obj.ttSimBundleNormal;
        end

        function RSME = getRSME(obj)
            realVest = obj.correlateFibers;

            estimate = realVest(:,1:2);
            real     = realVest(:,3:4);

            RSME = sqrt(mean(vecnorm(estimate-real, 2, 2).^2));
        end

        function nprocessd = getNprocessd(obj)
            nprocessd = obj.nPatternsProcessed;
        
        end

        function obj = printStatus(obj,ind,max)
            % Each \b removes one character from the previous fprintf.
            fprintf('\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\bCurr Row : %5d/%5d', ind,max);

        end

    end


end