classdef HexagonalBundle < FiberBundle
    methods
        function obj = HexagonalBundle(FiberSim)

            obj.FiberSim = FiberSim;

            ydist = FiberSim.d_fiber * sin(pi/3);

            nx = floor(FiberSim.bundle_len / FiberSim.d_fiber);
            ny = floor(FiberSim.bundle_len/ydist);

            if(mod(ny,2) == 1)
                ny = ny +1;
            end


            xcoord = (0:FiberSim.d_fiber:(nx-1)*FiberSim.d_fiber)+FiberSim.d_fiber/2;
            ycoord = (0:ydist:(ny-1)*ydist)+ydist/2;

            [X,Y] = ndgrid(xcoord,ycoord);

            % jede zweite Zeile wird um d_fiber/2 nach rechts verschoben
            offset = repmat([repmat(FiberSim.d_fiber/2,nx,1),zeros(nx,1)],1,ny/2);
            X = X + offset;


            obj.X = X(:)+FiberSim.padding;
            obj.Y = Y(:)+FiberSim.padding;
            obj.r = repmat(FiberSim.r_core,numel(X),1);
            obj.lum =  zeros(numel(X),1);

            obj.Permutation = randperm(length(obj.X))';

            obj.lumnoise = ones(numel(X),1);





            
        end
    end
end