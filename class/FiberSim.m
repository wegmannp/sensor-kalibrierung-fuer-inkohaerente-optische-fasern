
classdef  FiberSim < handle
    properties

        %% Glasfaser Parameter

        % Glasfaser Kern Radius in [m]
        r_core = 7e-6/2;

        % Dicke der Ummantelung der Glasfaser in [m]
        r_coating = 2e-6;

        % Durchmesser einer Faser in [m]
        d_fiber;

        % Seitenlänge des Bündels in [m]
        bundle_len = 0.3e-3;


        % Padding um das Bündel in [m]
        padding;

        %% Sensor Parameters

        % Seitenlänge des Viewports in [m]
        viewport_len;

        % Auflösung Sensor des viewports (Pixel)
        sensor_resolution = 1000;

        % Breite eines Sensorpixels in [m]
        sensor_pixel_width;

        % Breite der Scanline in [m]
        scanline_width;

        % Umrechnungsfaktor [m] --> Pixel
        factor_to_sensor_pxls;

        %% Pattern Parameters

        % (vor den Fasern) Maximale spatiale Auflösung in [m]
        source_res = 0.1e-6;

        % Auflösung des Musters vor den Fasern
        res_Pattern ;

        % Umrechnungsfaktor [m] --> Pixel
        factor_to_pxls ;
        
        % indicates if circle detection is turned of for testing of calib
        % methods
        use_exact_fiberCenters;
        
        % output progress?
        headless = true;

        pointSolvTimeOnly = false;
        
        sensorNoise = false;
        sensorNoiseVar = 0.01;



    end

    methods
        function obj = FiberSim(r_core,r_coating,bundle_len,res_Pattern,sensor_resolution,exact_fib_centers)
            obj.r_core            = r_core;
            obj.r_coating         = r_coating;
            obj.bundle_len        = bundle_len;
            obj.res_Pattern       = cast(res_Pattern,'int64');
            obj.sensor_resolution = sensor_resolution;
            obj.use_exact_fiberCenters = exact_fib_centers;

            
            obj.d_fiber =  (obj.r_core + obj.r_coating)*2;

            obj.padding = obj.d_fiber*2;

            obj.viewport_len = obj.bundle_len + 2 * obj.padding;

            obj.sensor_pixel_width = obj.viewport_len/obj.sensor_resolution;

            obj.scanline_width = obj.d_fiber;

            obj.factor_to_sensor_pxls = 1/obj.sensor_pixel_width;

            
            obj.source_res = obj.viewport_len/cast(obj.res_Pattern,'double'); 
            
          

            obj.factor_to_pxls = 1/obj.source_res;
        end



        function  setLen(obj,bundle_len)

            obj.bundle_len = bundle_len;
            
            obj.reCalcValues;

        
        end

        function set_r_core(obj,r_core)
            obj.r_core = r_core;
            obj.reCalcValues;
        
        end

        function set_r_coating(obj,r_coating)
            obj.r_coating = r_coating;
            obj.reCalcValues;
        
        end

        function set_headless(obj,headless)
            obj.headless = headless;
        end

              
        function set_psolv_time_only(obj,Tonly)
            obj.pointSolvTimeOnly = Tonly;
        end

        function set_Noise(obj,useNoise)
            obj.sensorNoise = useNoise;
        end

        function set_NoiseVar(obj,NoiseVar)
            obj.sensorNoiseVar = NoiseVar;
        end


    end

    methods (Access = private)
        function reCalcValues(obj)
            obj.d_fiber =  (obj.r_core + obj.r_coating)*2;

            obj.padding = obj.d_fiber * 2;

            obj.viewport_len = obj.bundle_len + 2 * obj.padding;

            obj.sensor_pixel_width = obj.viewport_len/obj.sensor_resolution;

            obj.scanline_width = obj.d_fiber;

            obj.factor_to_sensor_pxls = 1/obj.sensor_pixel_width;

            obj.res_Pattern = cast(obj.viewport_len/obj.source_res,"int64");

            obj.factor_to_pxls = 1/obj.source_res;

        end

    end
end