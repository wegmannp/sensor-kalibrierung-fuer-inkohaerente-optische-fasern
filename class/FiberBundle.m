classdef FiberBundle < handle

    properties
        X     % x coords of Fibers
        Y     % y coords of Fibers

        r     % radii of Fibers

        lum   % luminance of Fiber

        Permutation % random permutation of Fibers

        lum_factor = 1; % correction factor to normalize luminance to 0-255

        lumnoise 

        SensorImage % resulting image in the sensor

        FiberSim;
    end

    methods
        function obj = setLumNoise(obj,mu,sd)
            nFibs = numel(obj.X);

            obj.lumnoise = sd.* randn(nFibs,1) + mu ;
        end

        function obj= simulateBundle(obj,Pattern_mat)
            obj.lum = obj.generate_luminosity(Pattern_mat);
            obj.SensorImage  = obj.generateImage(obj.Permutation);
        end

        function obj = simulateBundle_ordered(obj,Pattern_mat)
            obj.lum = obj.generate_luminosity(Pattern_mat);
            obj.SensorImage = obj.generateImage(1:length(obj.X));
        end


        function [obj, imageseries, imageseries_ordered] = simulateBundleSeries(obj, PatternSeries)

            nPatterns = size(PatternSeries,3);
            imageseries         = cell(nPatterns,1);
            imageseries_ordered = cell(nPatterns,1);


            for i = 1:nPatterns
                obj.lum = obj.generate_luminosity(PatternSeries(:,:,i));

                imageseries(i,1)          = {obj.generateImage(obj.Permutation)};
                imageseries_ordered(i,1)  = {obj.generateImage(1:length(obj.X))};

            end

        end


    end

    methods (Access = private)
        function image = generateImage(obj,permutation)
            % generate Image at source Resolution

            factor = cast(obj.FiberSim.res_Pattern,'double')/obj.FiberSim.viewport_len;

            % map to pixel coordinates
            circs = [obj.X , obj.Y , obj.r] .* factor;


            blank_img = 0 * ones(obj.FiberSim.res_Pattern, obj.FiberSim.res_Pattern, 'uint8');

            image = insertShape(blank_img,"FilledCircle",circs,'Color',repmat(obj.lum(permutation),1,3),Opacity=1);

            image = imresize(image, [obj.FiberSim.sensor_resolution , obj.FiberSim.sensor_resolution]);
            image = rgb2gray(image);

            if obj.FiberSim.sensorNoise
            image = imnoise(image,'gaussian',0,obj.FiberSim.sensorNoiseVar);
            end

        end


        function luminosity = generate_luminosity(obj,Pattern_Mat)
            res_Pattern = obj.FiberSim.res_Pattern;
            luminosity = zeros(length(obj.X),1);

            % factor to pixel coordinates of pattern
            factor = cast(res_Pattern/obj.FiberSim.viewport_len,'double');



            for i = 1:length(obj.X)

                radius = round(obj.r(i,1) / obj.FiberSim.source_res);


                centerX = round(obj.Y(i,1) .* factor);
                centerY = round(obj.X(i,1) .* factor);

                sub_mat_len = 2*radius + 1;

                submat_pattern = Pattern_Mat(centerX-radius:centerX+radius,centerY-radius:centerY+radius);

                [columnsInImage, rowsInImage] = meshgrid(1:sub_mat_len, 1:sub_mat_len);

                % calculate Circle Mask
                circlePixels = (rowsInImage - radius-1).^2 ...
                    + (columnsInImage - radius-1).^2 <= radius.^2;

                % calculate amount of light that is transmitted by the fiber
                fiber_transmit = submat_pattern .* circlePixels;


                fiber_louminosity = trapz(trapz(fiber_transmit,2))/ sum(circlePixels,"all");

                luminosity(i,1) = fiber_louminosity;

            end

            %luminosity = luminosity .* obj.lum_factor;
            luminosity = luminosity .* obj.lumnoise;
        end

    end


end