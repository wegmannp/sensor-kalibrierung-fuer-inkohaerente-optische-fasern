classdef LineSolver < Solver

    properties


        % scanbox fiberwidth fraction
        sampling_r = 4;
        FibwFrac = 0.9;
    end
    properties (Access = private)
        LineB       % half the line width in [m]
        LineBpx     % half the line width in [source_pxls]
    end

    methods
        %constructor
        function obj = LineSolver(Bundle, FiberSim,sampling_r,FibwFrac)
            alltemp = tic;
            obj.FiberSim  = FiberSim;
            obj.Bundle    = Bundle;
            obj.sampling_r = sampling_r;
            obj.FibwFrac = FibwFrac;

            obj.SensorIm = zeros(obj.FiberSim.sensor_resolution,obj.FiberSim.sensor_resolution);
            obj.SourceIm = zeros(obj.FiberSim.res_Pattern,obj.FiberSim.res_Pattern);

            obj.LineB = FiberSim.d_fiber *  obj.FibwFrac * 0.5;

            obj.LineBpx = floor(obj.LineB *obj.FiberSim.factor_to_pxls);


            fiberwidth_px = obj.FiberSim.d_fiber * obj.FiberSim.factor_to_pxls;

            Coords = 2*obj.LineBpx : floor(fiberwidth_px/obj.sampling_r): obj.FiberSim.res_Pattern - 2*obj.LineBpx;


            % fiber positions on Proximal end
            % (generate empty LUT)
            obj.SourceIm(:) = 255;

            obj.Bundle.simulateBundle(obj.SourceIm);
            obj.SensorIm = obj.Bundle.SensorImage;

            % find Fiber Centers
            if ~obj.FiberSim.use_exact_fiberCenters
                [centers, radii] = obj.FDDT(obj.SensorIm);
            else
                centers = [obj.Bundle.X,obj.Bundle.Y] .* obj.FiberSim.factor_to_sensor_pxls;
                radii   = [obj.Bundle.r] .* obj.FiberSim.factor_to_sensor_pxls;
            end

            if(isempty(centers))
                error("no fiber centers found in sensor Image sensor resolution igit s too small ")
            end


            obj.DetectedVBundle = VirtualBundle( ...
                centers(:,1), ...
                centers(:,2), ...
                radii, ...
                zeros(length(centers),1), ...
                obj.FiberSim, ...
                obj.FiberSim.sensor_resolution ...
                );


            obj.SampleIndices = sub2ind( ...
                [obj.FiberSim.sensor_resolution, obj.FiberSim.sensor_resolution, 1], ...
                round(obj.DetectedVBundle.Y(:,1)), ...
                round(obj.DetectedVBundle.X(:,1)));




            % reset source to 0
            obj.SourceIm(:) = 0;

            X_distal  = zeros(numel(obj.DetectedVBundle.X),1);
            Y_distal  = zeros(numel(obj.DetectedVBundle.Y),1);
            lum_Bench = zeros(numel(obj.DetectedVBundle.Y),1);

            % Vertical Line --> x coords
            obj.ttSimBundle = 0;
            i = 0;
            obj.nPatternsProcessed = 0;
            for x = Coords

                obj.SourceIm(:,x-obj.LineBpx:x+obj.LineBpx) = 255;

                %imshow(obj.SourceIm)
                simbundletemp = tic;
                obj.Bundle = obj.Bundle.simulateBundle(obj.SourceIm);
                obj.nPatternsProcessed = obj.nPatternsProcessed +1;

                obj.SensorIm = obj.Bundle.SensorImage;
                obj.ttSimBundle = obj.ttSimBundle + toc(simbundletemp);

                % sample at fiber centers
                lum  = obj.SensorIm(obj.SampleIndices);

                I = lum > lum_Bench;

                lum_Bench(I) = lum(I);

                X_distal(I) = x * (obj.FiberSim.sensor_resolution / cast(obj.FiberSim.res_Pattern,'double'));
                %Y_distal(I) = y * (obj.FiberSim.sensor_resolution / cast(obj.FiberSim.res_Pattern,'double'));


                obj.SourceIm(:,x-obj.LineBpx:x+obj.LineBpx) = 0;
               
                if ~obj.FiberSim.headless
                    i = i +1;
                    obj.printStatus(i,numel(Coords)*2);
                end


            end

            %reset lum benchmark
            lum_Bench(:) = 0;

            % Horizontal Line --> y coords
            for y = Coords

                obj.SourceIm(y-obj.LineBpx:y+obj.LineBpx,:) = 255;

                %imshow(obj.SourceIm)
                simbundletemp = tic;
                obj.Bundle = obj.Bundle.simulateBundle(obj.SourceIm);
                obj.nPatternsProcessed = obj.nPatternsProcessed +1;
                obj.SensorIm = obj.Bundle.SensorImage;
                obj.ttSimBundle = obj.ttSimBundle + toc(simbundletemp);

                % sample at fiber centers
                lum  = obj.SensorIm(obj.SampleIndices);

                I = lum > lum_Bench;

                lum_Bench(I) = lum(I);

                %X_distal(I) = x * (obj.FiberSim.sensor_resolution / cast(obj.FiberSim.res_Pattern,'double'));
                Y_distal(I) = y * (obj.FiberSim.sensor_resolution / cast(obj.FiberSim.res_Pattern,'double'));


                obj.SourceIm(y-obj.LineBpx:y+obj.LineBpx,:) = 0;

                if ~obj.FiberSim.headless
                    i = i +1;
                    obj.printStatus(i,numel(Coords)*2);
                end


            end

            obj.ReconstructedVBundle = VirtualBundle( ...
                X_distal, ...
                Y_distal, ...
                obj.DetectedVBundle.r, ...
                zeros(obj.DetectedVBundle.len,1), ...
                obj.FiberSim, ...
                obj.FiberSim.sensor_resolution);

            obj.ttSimBundleNormal = obj.ttSimBundle;
            obj.ttKalib = toc(alltemp);
        end

    end
end