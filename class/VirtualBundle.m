classdef VirtualBundle
    properties
        X
        Y
        r
        lum
        len
        FiberSim

        maxRes

        sigma = 0.02 %width of Gaussian (1/e half-width)
        lumfactor = 0.61
    end

    methods
        function obj = VirtualBundle(X,Y,r,lum,FiberSim,maxRes)
            obj.X = X;
            obj.Y = Y;
            obj.r = r;
            obj.lum = lum;
            obj.len = length(obj.X);
            obj.FiberSim = FiberSim;
            obj.maxRes = maxRes;
        end

        function image = draw(obj)

            % map to pixel coordinates
            circs = [obj.X , obj.Y , obj.r];

            blank_img = zeros(obj.FiberSim.sensor_resolution, obj.FiberSim.sensor_resolution, 'uint8');

            image = insertShape(blank_img,"FilledCircle",circs,'Color',repmat(obj.lum,1,3),Opacity=1);

            image = rgb2gray(image);
        end

        function image = draw_rect(obj,s)

            % map to pixel coordinates
            rect = [obj.X , obj.Y , repmat(s,numel(obj.X),2)];

            blank_img = zeros(obj.FiberSim.sensor_resolution, obj.FiberSim.sensor_resolution, 'uint8');

            image = insertShape(blank_img,"FilledRectangle",rect,'Color',repmat(obj.lum,1,3),Opacity=1);

            image = rgb2gray(image);
        end

        function image = gaussFold(obj)
            wDeg = 1;  %size of image (in degrees)
            nPix = obj.FiberSim.d_fiber*4.4 * obj.FiberSim.factor_to_sensor_pxls;  %resolution of image (pixels);

            [xf,yf] = meshgrid(linspace(-wDeg/2,wDeg/2,nPix+1));
            xf = xf(1:end-1,1:end-1);
            yf = yf(1:end-1,1:end-1);


            Gaussian = exp(-(xf.^2+yf.^2)/obj.sigma^2);





            blank_img = zeros(obj.FiberSim.sensor_resolution, obj.FiberSim.sensor_resolution);



            lumIndices = sub2ind( ...
                [obj.FiberSim.sensor_resolution, obj.FiberSim.sensor_resolution, 1], ...
                round(obj.Y(:,1)), ...
                round(obj.X(:,1)));

            blank_img(lumIndices) = obj.lum;




            filtImg = conv2(blank_img,Gaussian,'same');

            image =cast(filtImg*obj.lumfactor,'uint8') ;
        end

        function image = interpolate(obj)


            [xq,yq] = meshgrid(1:obj.FiberSim.sensor_resolution,1:obj.FiberSim.sensor_resolution);


            vq = griddata(obj.X,obj.Y,cast(obj.lum,'double'),xq,yq,'linear');

            vq(isnan(vq))=0;
            image = cast(vq,'uint8');


        end


        function image = address(obj)

            matRes = obj.maxRes;

            Xscaled = obj.X .* (obj.maxRes/obj.FiberSim.sensor_resolution);
            Yscaled = obj.Y .* (obj.maxRes/obj.FiberSim.sensor_resolution);

            [xq,yq] = meshgrid(1:matRes,1:matRes);

            vq = griddata(Xscaled,Yscaled,cast(obj.lum,'double'),xq,yq,'natural');

            vq(isnan(vq))=0;
            image = cast(vq,'uint8');



        end

        function image = address_vanilla(obj)

            Xscaled =  obj.X .* (obj.maxRes/obj.FiberSim.sensor_resolution);
            Yscaled =  obj.Y .* (obj.maxRes/obj.FiberSim.sensor_resolution);


            blank_img = zeros(obj.maxRes,obj.maxRes);

            lumIndices = sub2ind( ...
                [obj.maxRes, obj.maxRes, 1], ...
                round(Yscaled), ...
                round(Xscaled));

            blank_img(lumIndices) = obj.lum;

            image = cast(blank_img,'uint8');


        end


        function image = address_fast(obj)

            Xscaled =  obj.X .* (obj.maxRes/obj.FiberSim.sensor_resolution);
            Yscaled =  obj.Y .* (obj.maxRes/obj.FiberSim.sensor_resolution);

            [xq,yq] = meshgrid(1:obj.maxRes,1:obj.maxRes);



            vq = griddata(Xscaled,Yscaled,cast(obj.lum,'double'),xq,yq,'linear');

            vq(isnan(vq))=0;
            
            image = cast(vq,'uint8');




        end

        function image = prioNeighbor(obj)

            Xscaled =  round(obj.X .* (obj.maxRes/obj.FiberSim.sensor_resolution));
            Yscaled =  round(obj.Y .* (obj.maxRes/obj.FiberSim.sensor_resolution));
            

            xmin = min(Xscaled);
            xmax = max(Xscaled);
            ymin = min(Yscaled);
            ymax = max(Yscaled);



            blank_img = NaN(obj.maxRes,obj.maxRes);



            lumIndices = sub2ind( ...
                [obj.maxRes, obj.maxRes, 1], ...
                round(Yscaled), ...
                round(Xscaled));


            blank_img([1:ymin-1,ymax+1:obj.maxRes],:) = 0;
            blank_img(:,[1:xmin-1,xmax+1:obj.maxRes]) = 0;


            blank_img(lumIndices) = obj.lum;


            [row, col] = find(isnan(blank_img));

            
             IPVTable = [row,col,zeros(numel(row),1)];

            





            for i = 1:length(IPVTable)

                msclice = blank_img( IPVTable(i,1)-1 : IPVTable(i,1)+1, IPVTable(i,2)-1 : IPVTable(i,2)+1   );

                IPVTable(i,3) = 9 - sum(isnan(msclice),'all');
            
            end
            
            IPVTable =  sort(IPVTable,'descend');

            for j = 1:length(IPVTable)

                msclice = blank_img( IPVTable(j,1)-1 : IPVTable(j,1)+1, IPVTable(j,2)-1 : IPVTable(j,2)+1   );


                blank_img(IPVTable(j,1),IPVTable(j,2)) = mean(msclice,'all','omitnan');
            
            end

            





            image = cast(blank_img,'uint8');

        end

        function point = getCenter(obj,i)
            point= [obj.X(i,1) , obj.Y(i,1)];
        end
    end
end