classdef ZufallsBundle < FiberBundle
    methods
        function obj = ZufallsBundle(FiberSim,percentage)

            obj.FiberSim = FiberSim;

            r_core_source = FiberSim.r_core * FiberSim.factor_to_pxls;


            fr_size = cast(FiberSim.res_Pattern,'double')-(2 * FiberSim.padding * FiberSim.factor_to_pxls);

            S.frameSize = [fr_size,fr_size]; % axis size, centered at (0,0)
            S.circSize = [r_core_source*(1-percentage),r_core_source, r_core_source*(1+percentage)];          % circle radius
            %S.circSize = [d_fib_source d_fib_source ];          % circle radius
            S.nSizes = NaN;
            S.maxCircsPerRad = inf;
            S.edgeType = 1;          % Frame should cut off circle edges
            S.supressWarning = true;
            S.drawFrame = true;      % show the black axis frame
            S.overlapType = 'absolute';
            S.overlap = FiberSim.r_coating * FiberSim.factor_to_pxls;
            S.drawFrame = false;
            [circData,~,~,~ ] = bubblebath(S);

       

            

            obj.X = (circData(:,1)+cast(FiberSim.res_Pattern,'double')*0.5)/FiberSim.factor_to_pxls;
            obj.Y = (circData(:,2)+cast(FiberSim.res_Pattern,'double')*0.5)/FiberSim.factor_to_pxls;
            obj.r = circData(:,3)/FiberSim.factor_to_pxls;
            obj.lum =  zeros(numel(obj.X),1)/FiberSim.factor_to_pxls;

            obj.Permutation = randperm(length(obj.X))';

            obj.lumnoise = ones(numel(obj.X),1);

        end
    end
end