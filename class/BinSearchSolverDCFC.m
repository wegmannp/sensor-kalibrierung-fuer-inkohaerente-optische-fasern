classdef BinSearchSolverDCFC < BinSearchSolver
    properties


        target_fiber_to_bar_ratio = 0.4;
        patternScaling;
        bar_width_noScaling;

    end

    methods
        function obj = BinSearchSolverDCFC(Bundle,FiberSim,target)


            obj.Bundle   = FiberBundle;

            obj.Bundle   = Bundle;
            obj.FiberSim = FiberSim;

            obj.target_fiber_to_bar_ratio = target;
            obj.maxPow2 = ceil(log2(obj.FiberSim.viewport_len/(obj.FiberSim.d_fiber/4)));

            obj.n_bars = 2^obj.maxPow2;

            obj.bar_width_noScaling = obj.FiberSim.viewport_len /2^obj.maxPow2;
            obj.patternScaling = (obj.target_fiber_to_bar_ratio * obj.FiberSim.d_fiber)/obj.bar_width_noScaling;
            obj.bar_width = obj.patternScaling * obj.bar_width_noScaling;

            % empty source and sensor Image
            obj.SensorIm = zeros(obj.FiberSim.sensor_resolution,obj.FiberSim.sensor_resolution);
            obj.SourceIm = zeros(obj.FiberSim.res_Pattern,obj.FiberSim.res_Pattern);

            caltemp   = tic;
            shifttemp = tic;
            obj = obj.calNormal();
            obj.ttKalib =  toc(caltemp);
            obj = obj.calShift();
            obj.ttKalibShift = toc(shifttemp);

        end




        function obj = calNormal(obj)

            % empty source and sensor Image
            obj.SensorIm = zeros(obj.FiberSim.sensor_resolution,obj.FiberSim.sensor_resolution);
            obj.SourceIm = zeros(obj.FiberSim.res_Pattern,obj.FiberSim.res_Pattern);

            obj.SourceIm = obj.vertbars(0);

            obj.Bundle.simulateBundle(obj.SourceIm);
            obj.SensorIm = obj.Bundle.SensorImage;

            if ~obj.FiberSim.use_exact_fiberCenters
                [centers, radii] = obj.FDDT(obj.SensorIm);
            else
                centers = [obj.Bundle.X,obj.Bundle.Y] .* obj.FiberSim.factor_to_sensor_pxls;
                radii   = [obj.Bundle.r] .* obj.FiberSim.factor_to_sensor_pxls;
            end
            if(isempty(centers))
                error("no fiber centers found in sensor Image sensor resolution is too small ")
            end


            obj.DetectedVBundle = VirtualBundle(centers(:,1),centers(:,2),radii,zeros(length(centers),1),obj.FiberSim,obj.FiberSim.sensor_resolution);

            %linear sample indcex of fiber centers on proximal end
            obj.SampleIndices = sub2ind( ...
                [obj.FiberSim.sensor_resolution, obj.FiberSim.sensor_resolution, 1], ...
                round(obj.DetectedVBundle.Y(:,1)), ...
                round(obj.DetectedVBundle.X(:,1)));

            obj.BinArrX = [];
            obj.BinArrY = [];

            pow2 = 0;
            obj.ttSimBundle = 0;
            obj.nPatternsProcessed = 0;
            while pow2 <= obj.maxPow2

                % generate source image
                obj.SourceIm = obj.vertbars(pow2);

                % X Coordinate
                binLumX = obj.genBinLumDiff(obj.SourceIm);



                % Y Coordinate
                binLumY = obj.genBinLumDiff(obj.SourceIm');



                obj.BinArrX = [obj.BinArrX,binLumX];
                obj.BinArrY = [obj.BinArrY,binLumY];


                if ~obj.FiberSim.headless
                    obj.printStatus(pow2,obj.maxPow2);
                end
                pow2 = pow2 +1;

            end

            obj.ttSimBundleNormal = obj.ttSimBundle;
            obj.ttSimBundle = 0;

            [coordsX,AdressX] =  getCoords(obj,obj.BinArrX);
            [coordsY,AdressY] =  getCoords(obj,obj.BinArrY);

            X = obj.clampCoords( coordsX);
            Y = obj.clampCoords( coordsY);



            %% vBudle mapped so sensor
            obj.ReconstructedVBundle = VirtualBundle( ...
                X', ...
                Y', ...
                obj.DetectedVBundle.r, ...
                zeros(obj.DetectedVBundle.len,1), ...
                obj.FiberSim,...
                2^obj.maxPow2);

            %% vBundleFromAdresses
            obj.ReconstructedVBundle_Addr = VirtualBundle( ...
                AdressX, ...
                AdressY, ...
                obj.DetectedVBundle.r, ...
                zeros(obj.DetectedVBundle.len,1), ...
                obj.FiberSim, ...
                2^obj.maxPow2);
        end

        function obj = calShift(obj)
            %% make x y coords more precise

            % how many Pattern Pixels equal one sensor Pixel:
            obj.stepsize = round(obj.FiberSim.sensor_pixel_width/(obj.FiberSim.source_res));

            barw_patt = round((obj.bar_width/2)*obj.FiberSim.factor_to_pxls);

           obj.circshiftSteps =  unique( round(linspace(-barw_patt,barw_patt,10)));
            if numel(obj.circshiftSteps) <= 1
                error('increase Sensor Resolution!')
            end


            obj.shiftLumX = zeros(obj.DetectedVBundle.len,numel(obj.circshiftSteps));
            obj.shiftLumY = zeros(obj.DetectedVBundle.len,numel(obj.circshiftSteps));

            pattern = obj.vertbars(obj.maxPow2);
            obj.ttSimBundle = 0;
            obj.nPatternsProcessedShift = 0;
            for i = 1:numel(obj.circshiftSteps)
                pattx = circshift(pattern , obj.circshiftSteps(1,i),2);
                patty = circshift(pattern , obj.circshiftSteps(1,i),2)';


                %x-Direction
                xtemp = tic;
                obj.Bundle = obj.Bundle.simulateBundle(pattx);
                obj.nPatternsProcessedShift = obj.nPatternsProcessedShift +1;
                obj.SensorIm = obj.Bundle.SensorImage;
                obj.ttSimBundle = obj.ttSimBundle + toc(xtemp);
                % sample lum at fiber centers
                lumx  = obj.SensorIm(obj.SampleIndices);

                % y-Direction
                ytemp =  tic;
                obj.Bundle = obj.Bundle.simulateBundle(patty);
                obj.nPatternsProcessedShift = obj.nPatternsProcessedShift +1;
                obj.SensorIm = obj.Bundle.SensorImage;
                obj.ttSimBundle = obj.ttSimBundle + toc(ytemp);
                % sample lum at fiber centers
                lumy  = obj.SensorIm(obj.SampleIndices);

                obj.shiftLumX(:,i) = lumx;
                obj.shiftLumY(:,i) = lumy;
                if ~obj.FiberSim.headless
                    obj.printStatus(i,numel(obj.circshiftSteps));
                end
            end
            obj.ttSimBundleShift = obj.ttSimBundle;
            obj.ttSimBundle = 0;

            [~, imax_x] = max(obj.shiftLumX,[],2 );
            [~, imax_y] = max(obj.shiftLumY,[],2 );

            [~, imin_x] = min(obj.shiftLumX,[],2 );
            [~, imin_y] = min(obj.shiftLumY,[],2 );



            shiftstepsX =  imax_x .* obj.BinArrX(:,obj.maxPow2+1);
            shiftstepsX = shiftstepsX + (imin_x .* ~obj.BinArrX(:,obj.maxPow2+1));


            shiftstepsY =  imax_y .* obj.BinArrY(:,obj.maxPow2+1);
            shiftstepsY = shiftstepsY + (imin_y .* ~obj.BinArrY(:,obj.maxPow2+1));


            xOffset = (obj.circshiftSteps(shiftstepsX)' .* obj.FiberSim.source_res).*obj.FiberSim.factor_to_sensor_pxls;
            yOffset = (obj.circshiftSteps(shiftstepsY)' .* obj.FiberSim.source_res).*obj.FiberSim.factor_to_sensor_pxls;


            X = obj.clampCoords(obj.ReconstructedVBundle.X + xOffset);
            Y = obj.clampCoords(obj.ReconstructedVBundle.Y + yOffset);

            obj.ReconstructedVBundle_shift = VirtualBundle( ...
                X, ...
                Y, ...
                obj.DetectedVBundle.r, ...
                zeros(obj.DetectedVBundle.len,1), ...
                obj.FiberSim, ...
                2^obj.maxPow2);
        end



    end


    methods (Access = public)
        function img = vertbars(obj,pow2)
            scaledRes = floor(obj.patternScaling * cast(obj.FiberSim.res_Pattern,'double'));

            % 2^0 --> return white Image
            if pow2 == 0
                img = ones(obj.FiberSim.res_Pattern,obj.FiberSim.res_Pattern)*255;
                return;
            end

            % core that is repeated 2^pow2 times
            core = [1,0];

            flatPattern = repmat(core,1,2.^(pow2-1));


            % flatPattern is now scaled to SourceIm Size:
            % scaing in x-Dim
            img= repelem(flatPattern,1,ceil(scaledRes/power(2,pow2)));

            %precise scaling
            img = resample(img' ,scaledRes,length(img))';
            %binarization
            img(img<=0.5)= 0;
            img(img>0.5) = 1;

            start = floor((scaledRes - obj.FiberSim.res_Pattern)/2);
            img = img(:,start:start+obj.FiberSim.res_Pattern-1);

            img = repmat(img',1,obj.FiberSim.res_Pattern)' * 255;

        end

        function [coords,address_vec] = getCoords(obj,binArr)

            scaledRes = floor(obj.patternScaling * cast(obj.FiberSim.res_Pattern,'double'));
            offset = (scaledRes - cast(obj.FiberSim.res_Pattern,'double'))/2;



            bar_center_coords = fliplr(linspace(obj.bar_width/2 - offset,scaledRes-offset,obj.n_bars))';

            %% umrechnung von source auf sensor pixel koords
            factor_to_sens = obj.FiberSim.sensor_resolution / cast(obj.FiberSim.res_Pattern,'double');

            bar_Index = 2.^(obj.maxPow2-1:-1:0) * binArr(:,2:obj.maxPow2+1)';



            coords = (bar_center_coords(bar_Index+1))' .* factor_to_sens;

            address_vec = bar_Index + 1;
        end



    end




    methods (Static)

        function louminosity = sampleLum(point,Image)
            circX = round(point(1,1));
            circY = round(point(1,2));
            louminosity = Image(circY,circX);

        end

        function coords = GetCoords(binSearchArr,i_bars,bar_center_coords)
            bar_Index = 2.^(i_bars-1:-1:0) * binSearchArr(:,2:i_bars+1)';

            coords = bar_center_coords(bar_Index+1);
        end
    end



end