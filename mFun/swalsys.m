function y= swalsys(k)

if (bitand(k,k-1) ~= 0)
    error('k must be power of 2')
end
y = [];
for swalrow = 0:k-1
    res = swal(swalrow);
    repfactor = k/length(res);
    y = [y;repelem(res,repfactor)];

end
end

function y = swal(k)

kbin = dec2bin(k);

y = [1];

if(k == 0 )
    return;
end

for i = kbin

    if i == '1' %zentralsymmetrische Erweiterung
        y = [y fliplr(-1*y)];
    end

    if i == '0' %symmetrische Erweiterung
        y = [y fliplr(y)];
    end
end

end


