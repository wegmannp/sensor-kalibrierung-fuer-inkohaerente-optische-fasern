# Sensor Kalibrierung für inkohärente optische Fasern



## Demo Skript:

Das Matlab Skript ```main.mlx``` stellt alle in der Arbeit vorgestellten Kalibrierungsverfahren gegenüber und zeigt die komplette Pipeline von Fasserbündel-Parametrsierung, über Kalibrierung und bis hin zur Bildübertragung und Bildrekonstruktion. 

## Hauptsimulation für Konfiguration 1 & 2:

Das für die Gernerierung der in Abschnitt 5.2.1 gezeigten Messwerte verwendete Skript ```Haupsimulation_Konfig_1_und_2.mlx```. 


## Einfluss der Eingabemusterskalierung bei Flächencodierungsverfahren:

Erzeugung der Rohdaten und Abbildungen für die Analyse in Abschnitt 5.2.2 wurde mit dem Skript ```Eingabemusterskalierung.mlx``` durchgeführt.

##  Empfindlichkeit für Rauschen:

Erzeugung der Rohdaten und Abbildung für die Analyse in Abschnitt 5.2.3 wurde mit dem Skript ```Rausch_Empfindlichkeit.mlx``` durchgeführt.

# Ordnerstruktur:

- ```/class```
Matlab-Klassen für Simulationsumgebung

- ```/fig```
Verzeichnis in welchem erzeugte Abbildungen gespeichert werden.

- ```/mats```
Mat-Objekt Dateien in welchen Simulationsdaten gespeichert sind

- ```/mFun```
Hilfsfunktionen für Plots oder ähnliches
